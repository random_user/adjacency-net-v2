Welcome to adjacency-net-v2's documentation!
==============================================================================

Contents:

.. toctree::
   :maxdepth: 3

   readme
   installation
   usage
   notebooks
   contributing
   authors
   history

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
